#import "ParseServicePlugin.h"
#import <AudioToolbox/AudioToolbox.h>
#import "FCUUID.h"
#import "iosVersioning.h"
#import "AlertViewBlocks.h"

@interface ParseServicePlugin ()

@end

@implementation ParseServicePlugin

// The plugin must call super dealloc.
- (void) dealloc {
	[super dealloc];
}

// The plugin must call super init.
- (id) init {
	self = [super init];
	if (!self) {
		return nil;
	}
	return self;
}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
	@try {
        self.tealeafViewController = appDelegate.tealeafViewController;
        NSLog(@"{parseservice} initializeWithManifest");
        NSDictionary *ios = [manifest valueForKey:@"ios"];
		NSString *parseAppId = [ios valueForKey:@"parseAppId"];
		NSString *parseClientKey = [ios valueForKey:@"parseClientKey"];
		[Parse setApplicationId:parseAppId clientKey:parseClientKey];
        [PFAnalytics trackAppOpenedWithLaunchOptions:nil];
        
        PFUser *user = [PFUser user];
        user.username = [FCUUID uuidForDevice];
        user.password = user.username;
        user.email = [[user.username stringByAppendingString:@"@"] stringByAppendingString:[ios valueForKey:@"bundleID"]];
//        NSLog(@"{parseservice} PFUser email: %@",user.email);
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                if (!error) {
//                    //ok
//                } else {
//                    NSString *errorString = [error userInfo][@"error"];
//                    NSLog(@"{parseservice} error PFUser: %@",errorString);
//                    // Show the errorString somewhere and let the user try again.
//                }
                [PFUser logInWithUsernameInBackground:user.username password:user.password
                    block:^(PFUser *user, NSError *error) {
                        if (user) {
                            user[@"platformType"] = [[@"ios (" stringByAppendingString:get_platform()] stringByAppendingString:@")"];
                            user[@"platformVersion"] = [[UIDevice currentDevice] systemVersion];
                            user[@"appVersion"] = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                            [user saveInBackground];
                            NSLog(@"{parseservice} PFUser: The login successfully");
                            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                            currentInstallation[@"user"] = user;
                            [currentInstallation saveInBackground];
                            NSLog(@"{parseservice} PFInstallation: Associate the device with a user successfully");
                            // Do stuff after successful login.
                        } else {
                            // The login failed. Check error to see why.
                            NSLog(@"{parseservice} PFUser: The login failed");
                        }
                }];
        }];
        
	}
	@catch (NSException *exception) {
		NSLog(@"{parseservice} error init: %@",exception);
	}
}

- (void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken application:(UIApplication *)app {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void) didReceiveRemoteNotification:(NSDictionary *)userInfo application:(UIApplication *)app {
//    [PFPush handlePush:userInfo];
//    https://github.com/dkasper/Sightglass-Bingo/blob/master/sightglassbingo/Parse%203/PFPush.m
    
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(applicationState)] &&
        [application applicationState] != UIApplicationStateActive) {
        return;
    }
    NSDictionary *aps = [userInfo objectForKey:@"aps"];
    if ([aps objectForKey:@"alert"]) {
        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
        NSString *message = [aps objectForKey:@"alert"];
        
        if([aps objectForKey:@"uri"]){
            
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:appName message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert showAlerViewFromButtonAction:nil animated:YES handler:^(UIAlertView *alertView, NSInteger buttonIndex){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [aps objectForKey:@"uri"]]];
            }];
            [alert release];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: appName
                                               message: message
                                              delegate: nil
                                     cancelButtonTitle: @"OK"
                                     otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    if ([aps objectForKey:@"badge"]) {
        NSInteger badgeNumber = [[aps objectForKey:@"badge"] integerValue];
        [application setApplicationIconBadgeNumber:badgeNumber];
    }
    if ([aps objectForKey:@"sound"] &&
        ![[aps objectForKey:@"sound"] isEqualToString:@""] &&
        ![[aps objectForKey:@"sound"] isEqualToString:@"default"]) {
        NSString *soundName = [aps objectForKey:@"sound"];
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:[soundName stringByDeletingPathExtension]
                                                              ofType:[soundName pathExtension]];
        if (soundPath) {
            SystemSoundID soundId;
            AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:soundPath], &soundId);
            AudioServicesPlaySystemSound(soundId);
            return;
        }
    }
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void) getList:(NSDictionary *)jsonObject withRequestId:(NSNumber *)requestId {
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    float frameWidth = self.tealeafViewController.view.frame.size.width;
    float frameHeight = self.tealeafViewController.view.frame.size.height;
    activityView.frame = CGRectMake(
                                    (frameWidth - activityView.frame.size.width)/2,
                                    (frameHeight - activityView.frame.size.height)/2,
                                    activityView.frame.size.width,
                                    activityView.frame.size.height
    );
    
    [self.tealeafViewController.view addSubview:activityView];
    [activityView startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    NSString *className = [jsonObject objectForKey:@"className"];
    PFQuery *query = [PFQuery queryWithClassName:className];
	query.limit = 1000;
	[query whereKey:@"active" equalTo:@true];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSDictionary *response = nil;
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:objects.count];
            int index = 0;
            for (PFObject *object in objects) {
                NSMutableDictionary *dicObj = [[NSMutableDictionary alloc] init];
                NSArray *keys = object.allKeys;
                for(NSString *key in keys){
                    if([[object objectForKey:key] isKindOfClass:[PFACL class]]){
                        continue;
                    }
                    if([[object objectForKey:key] isKindOfClass:[PFFile class]]){
                        PFFile *file = [object objectForKey:key];
                        [dicObj setObject:file.url forKey: key];
                    }else{
                        [dicObj setObject:[object objectForKey:key] forKey: key];
                    }
                }
                [list setObject:dicObj atIndexedSubscript: index++];
            }
            response = [NSDictionary dictionaryWithObjectsAndKeys: list,@"serverData", nil];
//            NSLog(@"========= serverObj %@", response);
            [[PluginManager get] dispatchJSResponse:response withError:error andRequestId:requestId];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            [[PluginManager get] dispatchJSResponse:nil withError: error andRequestId:requestId];
        }
        [activityView stopAnimating];
        [activityView removeFromSuperview];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
}

@end

