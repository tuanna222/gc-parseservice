var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var parseservice = Class(function () {
	this.init = function () {
	}

	/**
	* opts: {className: "Story", where: {name: ">=value"}, limit: 10}
	*/
	this.getList = function(opts, cb){
		NATIVE.plugins.sendRequest("ParseServicePlugin", "getList", opts, function (err, res) {
			cb && cb(err, res);
		});
	};

});

exports = new parseservice();