package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.EventQueue;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.Build;
import java.util.HashMap;
import java.lang.Character;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

import com.tealeaf.plugin.IPlugin;
import com.tealeaf.plugin.PluginManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.app.Dialog;
import android.widget.TextView;
import android.widget.Button;
import android.widget.TableLayout.LayoutParams;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.text.method.ScrollingMovementMethod;
import android.view.View;

import android.telephony.TelephonyManager;
import android.net.wifi.WifiManager;
import android.app.ProgressDialog;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;
import com.parse.*;


public class ParseServicePlugin implements IPlugin {
	Context context;
	public ParseServicePlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
		PackageManager manager = applicationContext.getPackageManager();
        try {
        	logger.log("{parseservice} init...");
            Bundle meta = manager.getApplicationInfo(applicationContext.getPackageName(), PackageManager.GET_META_DATA).metaData;
            String parseAppId = meta.getString("PARSE_APP_ID");
            String parseClientKey = meta.getString("PARSE_CLIENT_KEY");
            Parse.initialize(applicationContext, parseAppId, parseClientKey);
			// logger.log("{parseservice}",parseAppId,parseClientKey);
        } catch (Exception e) {
            logger.log("{parseservice} EXCEPTION",e.getMessage());
            e.printStackTrace();
        }
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
		context = activity;
		PackageManager manager = activity.getPackageManager();
		String parseAppPackage = "";
		String _appVersion = "";
		try {
            Bundle meta = manager.getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData;
            if (meta != null) {
                parseAppPackage = meta.getString("PARSE_APP_PACKAGE");
            }
			_appVersion = manager.getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (Exception e) {
            android.util.Log.d("{parseservice} EXCEPTION", "" + e.getMessage());
        }

		logger.log("{parseservice} SaveToken...");
		ParseInstallation.getCurrentInstallation().saveInBackground();
		logger.log("{parseservice} ParseAnalytics...");
		ParseAnalytics.trackAppOpened(activity.getIntent());
		//user
		// logger.log("{parseservice} get deviceid...");
		String m_deviceId = null;
		WifiManager m_wm = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
		m_deviceId = m_wm.getConnectionInfo().getMacAddress();

		if (m_deviceId == null) {
			TelephonyManager TelephonyMgr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE); 
			m_deviceId = TelephonyMgr.getDeviceId();
		}

		// logger.log("{parseservice} m_deviceId:", m_deviceId);

		if(m_deviceId != null){
			final String deviceId = this.generateMD5(m_deviceId);
			// logger.log("{parseservice} deviceId:", deviceId);
			ParseUser user = new ParseUser();
			user.setUsername(deviceId);
			user.setPassword(deviceId);
			user.setEmail(deviceId+"@"+parseAppPackage);
			final String appVersion = _appVersion;
			String release = Build.VERSION.RELEASE;
    		int sdkVersion = Build.VERSION.SDK_INT;
    		final String platformVersion = sdkVersion + " (" + release +")";
    		final String deviceName = this.getDeviceName();
			user.signUpInBackground(new SignUpCallback() {
			  	public void done(ParseException e) {
			  		if(e != null){
			  			logger.log("{parseservice} Error: ", e.getMessage());
			  		}
			  		ParseUser.logInInBackground(deviceId, deviceId, new LogInCallback() {
					  	public void done(ParseUser user, ParseException e) {
						    if (user != null) {
						    	// other fields can be set just like with ParseObject
								user.put("platformType", "android ("+deviceName+")");
								user.put("platformVersion", platformVersion);
								user.put("appVersion", appVersion);
								user.saveInBackground();
						    	logger.log("{parseservice} ParseUser: The user is logged in");
						    	ParseInstallation installation = ParseInstallation.getCurrentInstallation();
						    	installation.put("user", user);
								installation.saveInBackground();
								logger.log("{parseservice} ParseInstallation: Associate the device with a user successfully");
						    } else {
						    	logger.log("{parseservice} Parse: Login failed. Look at the ParseException to see what happened");
						    }
					  	}
					});
			  	}
			});
		}
	}

	public String getDeviceName() {
	   String manufacturer = Build.MANUFACTURER;
	   String model = Build.MODEL;
	   if (model.startsWith(manufacturer)) {
	      return this.capitalize(model);
	   } else {
	      return this.capitalize(manufacturer) + " " + model;
	   }
	}


	private String capitalize(String s) {
	    if (s == null || s.length() == 0) {
	        return "";
	    }
	    char first = s.charAt(0);
	    if (Character.isUpperCase(first)) {
	        return s;
	    } else {
	        return Character.toUpperCase(first) + s.substring(1);
	    }
	}

	public String generateMD5(String md5) {
	   try {
	        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
	        byte[] array = md.digest(md5.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < array.length; ++i) {
	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
	       }
	        return sb.toString();
	    } catch (java.security.NoSuchAlgorithmException e) {
	    }
	    return null;
	}

	public void getList(final String json, final Integer requestId) {
		TeaLeaf.get().runOnUiThread(new Runnable() {
			public void run() {
				final ProgressDialog loadingDialog = new ProgressDialog(context);
				loadingDialog.setMessage("Loading...");
		        loadingDialog.setIndeterminate(false);
		        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		        loadingDialog.setCancelable(false);
		        loadingDialog.show();
				try {
					JSONObject obj = new JSONObject(json);
					final String className = obj.getString("className");
					
					ParseQuery<ParseObject> query = ParseQuery.getQuery(className);
					query.setLimit(1000);
					query.whereEqualTo("active", true);
					query.findInBackground(new FindCallback<ParseObject>() {
					    public void done(List<ParseObject> list, ParseException e) {
					        if (e == null) {
					        	Log.d("{parseservice} ", "Retrieved " + list.size() + " objects");
					        	HashMap response = new HashMap();
					        	ArrayList newList = new ArrayList();
					        	for (int i = 0; i < list.size(); i++) {
					        		HashMap dicObj = new HashMap();
								    ParseObject pObj = (ParseObject) list.get(i);
								    Set keys = pObj.keySet();
								    Iterator<String> iterKeys = keys.iterator();
									while (iterKeys.hasNext()) {
										String key = iterKeys.next();
										if(key.equals("ACL")){
											continue;
										}
										if(pObj.get(key) instanceof ParseFile){
											ParseFile file = pObj.getParseFile(key);
											dicObj.put(key, file.getUrl());
										}else{
											dicObj.put(key, pObj.get(key));
										}
									    // Log.d("{parseservice} keySet", ""+ key);
									}
									newList.add(dicObj);
								}
								response.put("serverData", newList);
					            PluginManager.sendResponse(response, null, requestId);
					            loadingDialog.dismiss();
					        } else {
					            Log.d("{parseservice} ", "Error: " + e.getMessage());
					            PluginManager.sendResponse(null, e.getMessage(), requestId);
					            loadingDialog.dismiss();
					        }
					    }
					});
				} catch(Exception e) {
					logger.log("{parseservice} Exception while getting objects list:", e.getMessage());
					PluginManager.sendResponse(null, e.getMessage(), requestId);
					loadingDialog.dismiss();
				}
			}
		});
	}

	public void onResume() {
	}

	public void onStart() {
	}

	public void onPause() {
	}

	public void onStop() {
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}
}

