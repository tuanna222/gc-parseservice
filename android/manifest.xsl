<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:android="http://schemas.android.com/apk/res/android">

	<xsl:param name="parseAppId" />
	<xsl:param name="parseClientKey" />
	<xsl:param name="parseAppPackage" />
	
	<xsl:template match="meta-data[@android:name='PARSE_APP_ID']">
		<meta-data android:name="PARSE_APP_ID" android:value="{$parseAppId}"/>
	</xsl:template>
	<xsl:template match="meta-data[@android:name='PARSE_CLIENT_KEY']">
		<meta-data android:name="PARSE_CLIENT_KEY" android:value="{$parseClientKey}"/>
	</xsl:template>
	<xsl:template match="meta-data[@android:name='PARSE_APP_PACKAGE']">
		<meta-data android:name="PARSE_APP_PACKAGE" android:value="{$parseAppPackage}"/>
	</xsl:template>
	
	<xsl:template match="category[@android:name='com.parse.starter']">
		<category android:name="{$parseAppPackage}" />
	</xsl:template>
	<xsl:template match="permission[@android:name='com.parse.starter.permission.C2D_MESSAGE']">
		<permission android:protectionLevel="signature"
    android:name="{$parseAppPackage}.permission.C2D_MESSAGE" />
	</xsl:template>
	<xsl:template match="uses-permission[@android:name='com.parse.starter.permission.C2D_MESSAGE']">
		<uses-permission android:name="{$parseAppPackage}.permission.C2D_MESSAGE" />
	</xsl:template>

	<xsl:output indent="yes" />
	<xsl:template match="comment()" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
